# Input Format
#
# First line of input contains a single integer T denoting the number of test cases.
# First line of each test case contains two space separated integers N and M denoting the number of levels and number of enemies at each level respectively.
# Each of next N lines of a test case contain M space separated integers, where jth integer in the ith line denotes the power P of jth enemy on the ith level.
# Each of the next N lines of a test case contains M space separated integers, where jth integer in the ith line denotes the number of bullets B jth enemy of ith level has.
#
# 1 <= T <= 100
# 1 <= N <= 100
# 1 <= M <= 5*10^5
# 1 <= P, B <= 1000
# N*M can't exceed 5*10^5

def main():
    T = int(input())

    for t in range(T):
        N, M = [*map(int, input().split())]
        power = list()
        for i in range(N):
            power.append([*map(int, input().split())])
        bullets = list()
        for i in range(N):
            bullets.append([*map(int, input().split())])

        bullets_reqd = 0
        player_bullets = 0
        diff = list()
        for i in range(N):
            diff.append([])
            for j in range(M):
                diff[i].append(bullets[i][j] - power[i][j])
            earned_bullets, monster = greatest_diff(diff[i], power[i])
            if player_bullets - power[i][monster] < 0:
                bullets_reqd -= player_bullets - power[i][monster]
                player_bullets -= player_bullets - power[i][monster]
            player_bullets += earned_bullets
        print(bullets_reqd)

'''
Returns the greatest value for the list of differences between bullets and
power. If the same number is found more than once, returns the position of the
monster with the least power.
'''
def greatest_diff(diff, power):
    pos = 0
    val = diff[0]
    for i in range(1, len(diff)):
        if diff[i] > val:
            val = diff[i]
            pos = i
        elif diff[i] == val:
            if power[i] < power[pos]:
                val = diff[i]
                pos = i
    return [val, pos]

if __name__ == '__main__':
    main()
